# Flowchart Moodle Filter #

LMS Moodle filter to render/display flowcharts
It uses flowchart.js by Adriano Raiano (http://adrai.github.io/flowchart.js, MIT license) that uses Raphaël js (http://raphaeljs.com, MIT license)


### Using ###

common syntax is laTeX like:

	\flowChart[<options>]{<content>}

example:

	\flowChart[inline, yes-text:yup, line-width: 2]{st=>start: start
	in=>inputoutput: x,y
	op=>operation: z=x+y
	ou=>inputoutput: z
	en=>end: end
	st->in->op->ou->en}


### Syntax ###

Syntax of content of the command see in flowchart.js project (http://adrai.github.io/flowchart.js/).

There could be any options in the command.
Name them like options from example on the flowchart.js project page to make effect.
Both `:` and `=` could be name-value separator.

There are four extra option:

* background-color : <html/css color>
* align            : <html/css align>
* vertical-align   : <html/css vertical-align> // will effect onli for inline flowchart
* display          : <css display prperty>

Last three are not nessessery for manual operating.
Background-color will affect div container.
Displaying inline could be set up via single `inline`.


### Installation ###

*from moodle.org*

* checkout https://moodle.org/plugins/view/filter_flowchart (not available now)

*as zip*

* download as zip (see https://bitbucket.org/belomir/moodle-filter_flowchart/downloads)
* install to moodle system via admin interface

*manually*

* clone this repo
* name the folder by filter name (flowchart)
* copy this folder into moodle's filter folder on server


### How it works ###

It parses text to find all commands.
Actually, standart php part of filter changes command to proper for js wrapper first.
Then php part passes filter options from admin page.
Plugs in to page nessessery js's.
And runs main function from wrapper.

Wrapper finds all commands and parse them splitting options and content.
Both are parses too.
Places of commands appearing replaces with div container with proper settings.
And then it calls flowchart.js function to add svg flowchart to given container with parsed text and locally overwrited options.


### Todo: ###

* more standart options to settings