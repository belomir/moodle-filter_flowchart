VERSION = 0.72
BUILD_DATE = $(shell date +%Y%m%d%H)
DEPLOY_NAME = flowchart-${BUILD_DATE}.zip
WORK_DIR = build/flowchart

all: submodule update versioning deploy

# Initialises the submodule
submodule:
	git submodule init
	git submodule update

# Updates the submodule
update:
	cd vendor/flowchart.js; git pull origin master
	cd vendor/raphael; git pull origin master

# Updates version and build
versioning:
	sed -e 's/\(plugin->version[^=]*= \)[^;]*/\1${BUILD_DATE}/' -i version.php
	sed -e "s/\(plugin->release[^=]*= '\)[^';]*/\1${VERSION}/" -i version.php
	sed -e 's/\(\* ver\. \)[^ ]*/\1${VERSION}/' -i version.php
	sed -e 's/\(\* ver\. \)[^ ]*/\1${VERSION}/' -i filter.php
	sed -e 's/\(\* ver\. \)[^ ]*/\1${VERSION}/' -i filtersettings.php
	sed -e 's/\(\* ver\. \)[^ ]*/\1${VERSION}/' -i js/flowchart-wrapper.js

# Deploys the filter as a zip file
deploy:
	mkdir ${WORK_DIR}
	cp -r lang ${WORK_DIR}
	cp -r js ${WORK_DIR}
	cp vendor/flowchart.js/release/flowchart-1.4.0.min.js ${WORK_DIR}/js/flowchart-min.js
	cp vendor/raphael/raphael-min.js ${WORK_DIR}/js
	cp filter.php ${WORK_DIR}
	cp filtersettings.php ${WORK_DIR}
	cp GNU_GeneralPublicLicense.txt ${WORK_DIR}
	cp README.md ${WORK_DIR}
	cp version.php ${WORK_DIR}
	cp changelog ${WORK_DIR}
	cp thirdpartylibs.xml ${WORK_DIR}
	cd build && zip -r ${DEPLOY_NAME} flowchart
	rm -rf ${WORK_DIR}
