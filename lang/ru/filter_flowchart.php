<?php
/**
 ******************************************************************************
 *     __ _                   _                _      __ _ _ _                *
 *    / _| | _____      _____| |__   __ _ _ __| |_   / _(_) | |_ ___ _ __     *
 *   | |_| |/ _ \ \ /\ / / __| '_ \ / _` | '__| __| | |_| | | __/ _ \ '__|    *
 *   |  _| | (_) \ V  V / (__| | | | (_| | |  | |_  |  _| | | ||  __/ |       *
 *   |_| |_|\___/ \_/\_/ \___|_| |_|\__,_|_|   \__| |_| |_|_|\__\___|_|       *
 *                                                                            *
 ******************************************************************************
 * flowchart moodle filter             * Sergey Roganov © copyleft            *
 * uses flowchart.js by Adriano Raiano * Novosibirsk, 2015                    *
 * ver. 0.7                            * distributed under terms of           *
 *                                     * GNU/GPL ver.3 or above               *
 ******************************************************************************
 * filter syntax:                                                             *
 *     \flowChart[<options>]{<content>}                                       *
 ******************************************************************************
 * @package    filter
 * @subpackage flowchart
 * @copyright  2015 Sergey Roganov
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['filtername']       = 'блок-схемы';
$string['yes_text_name']    = 'подпись да';
$string['yes_text_desc']    = 'подпись истинной ветки ветвления';
$string['yes_text_default'] = 'да';
$string['no_text_name']     = 'подпись нет';
$string['no_text_desc']     = 'подпись ложной ветки ветвления';
$string['no_text_default']  = 'нет';
