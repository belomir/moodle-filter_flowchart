﻿<?php
/**
 ******************************************************************************
 *     __ _                   _                _      __ _ _ _                *
 *    / _| | _____      _____| |__   __ _ _ __| |_   / _(_) | |_ ___ _ __     *
 *   | |_| |/ _ \ \ /\ / / __| '_ \ / _` | '__| __| | |_| | | __/ _ \ '__|    *
 *   |  _| | (_) \ V  V / (__| | | | (_| | |  | |_  |  _| | | ||  __/ |       *
 *   |_| |_|\___/ \_/\_/ \___|_| |_|\__,_|_|   \__| |_| |_|_|\__\___|_|       *
 *                                                                            *
 ******************************************************************************
 * flowchart moodle filter             * Sergey Roganov © copyleft            *
 * uses flowchart.js by Adriano Raiano * Novosibirsk, 2015                    *
 * ver. 0.72                           * distributed under terms of           *
 *                                     * GNU/GPL ver.3 or above               *
 ******************************************************************************
 * filter syntax:                                                             *
 *     \flowChart[<options>]{<content>}                                       *
 ******************************************************************************
 * @package    filter
 * @subpackage flowchart
 * @copyright  2015 Sergey Roganov
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class filter_flowchart extends moodle_text_filter {
    public function filter($text, array $options = array()) {
        // change command to prevent filtering whet it must not be (editing)
        //     if command won't be changed, js wrapper won't get it
        $text = str_replace('\flowChart', '\TheFlowChart', $text);
        return $text;
    }
    public function setup($page, $context) {
        global $CFG;
        // plug in nessessary javascript since js can't be included in js plainly
        $page->requires->js(new moodle_url('/filter/flowchart/js/raphael-min.js'));
        $page->requires->js(new moodle_url('/filter/flowchart/js/flowchart-min.js'));
        // filter wrapper
        $page->requires->js(new moodle_url('/filter/flowchart/js/flowchart-wrapper.js'));
        // check if configuration strings are not empty
        if($CFG->filter_flowchart/yes_text != ''){
            $yestext = $CFG->filter_flowchart/yes_text;
        } else {
            $yestext = get_string('yes_text_default', 'filter_flowchart');
        }
        if($CFG->filter_flowchart/no_text != ''){
            $notext = $CFG->filter_flowchart/no_text;
        } else {
            $notext = get_string('no_text_default', 'filter_flowchart');
        }
        // options array for json serialization
        $options = array(
            'yes-text' => $yestext,
            'no-text'  => $notext
        );
        // json serialization to pass into javascript function
        $option_list = json_encode($options, JSON_UNESCAPED_UNICODE);
        // pass options
        $page->requires->js_init_call("initFlowcharts({$option_list});");
        // call filtering
        $page->requires->js_init_call('filterFlowcharts();');
    }
}
?>
