<?php
/**
 ******************************************************************************
 *     __ _                   _                _      __ _ _ _                *
 *    / _| | _____      _____| |__   __ _ _ __| |_   / _(_) | |_ ___ _ __     *
 *   | |_| |/ _ \ \ /\ / / __| '_ \ / _` | '__| __| | |_| | | __/ _ \ '__|    *
 *   |  _| | (_) \ V  V / (__| | | | (_| | |  | |_  |  _| | | ||  __/ |       *
 *   |_| |_|\___/ \_/\_/ \___|_| |_|\__,_|_|   \__| |_| |_|_|\__\___|_|       *
 *                                                                            *
 ******************************************************************************
 * flowchart moodle filter             * Sergey Roganov © copyleft            *
 * uses flowchart.js by Adriano Raiano * Novosibirsk, 2015                    *
 * ver. 0.72                           * distributed under terms of           *
 *                                     * GNU/GPL ver.3 or above               *
 ******************************************************************************
 * filter syntax:                                                             *
 *     \flowChart[<options>]{<content>}                                       *
 ******************************************************************************
 * @package    filter
 * @subpackage flowchart
 * @copyright  2015 Sergey Roganov
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();
$plugin->component = 'filter_flowchart'; // used for diagnostics
$plugin->version   = 2015042318;         // Date: YYYYMMDDXX
$plugin->requires  = 2014101000;         // Moodle 2.8.0 is required, but should work in any >2, 2.5 tested
$plugin->maturity  = MATURITY_RC;        // needs testing espesially in modern moodle versions
$plugin->release   = '0.72';             // it's in single master branch for today
