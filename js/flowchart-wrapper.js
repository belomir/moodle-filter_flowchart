﻿/**
 ******************************************************************************
 *     __ _                   _                _      __ _ _ _                *
 *    / _| | _____      _____| |__   __ _ _ __| |_   / _(_) | |_ ___ _ __     *
 *   | |_| |/ _ \ \ /\ / / __| '_ \ / _` | '__| __| | |_| | | __/ _ \ '__|    *
 *   |  _| | (_) \ V  V / (__| | | | (_| | |  | |_  |  _| | | ||  __/ |       *
 *   |_| |_|\___/ \_/\_/ \___|_| |_|\__,_|_|   \__| |_| |_|_|\__\___|_|       *
 *                                                                            *
 ******************************************************************************
 * flowchart moodle filter             * Sergey Roganov © copyleft            *
 * uses flowchart.js by Adriano Raiano * Novosibirsk, 2015                    *
 * wrapper for moodle filter           * distributed under terms of           *
 * ver. 0.72                           * GNU/GPL ver.3 or above               *
 ******************************************************************************
 *  definitions:                                                              *
 *      prefixes:                                                             *
 *          fc - flowchart parts                                              *
 *          re - regular explressions                                         *
 *          i  - integers                                                     *
 *      filter syntax:                                                        *
 *          \flowChart[<options>]{<content>}                                  *
 ******************************************************************************
 * flowchart filter wrapper for flowchart.js
 *
 * @package    filter
 * @subpackage flowchart
 * @copyright  2015 Sergey Roganov
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


// globals
var DEBUG              = true;           // debug mode
var fcPrefix           = '\\';           // LaTeX style
var fcCommand          = 'TheFlowChart'; // LaTeX style // this is trick
var fcOptionBrackets   = '[]';           // LaTeX style
var fcContentBrackets  = '{}';           // LaTeX style
var fcContent          = [];             // all founded flowchart commands array:
var iCommand           = 0;              // 	full command index
var iRawOptions        = 2;              // 	optionss index
var iRawContent        = 3;              // 	html content index
var iContent           = 4;              // 	parsed content index
var iFlowchart         = 5;              // 	container id index
var iOptions           = 6;              // 	parsed options
var fcHtmlContext;                       // html body to insert flowcharts
var fcHtmlContent;                       // it's html text
var fcOptions          = {};             // options array for flowchart.js

// just logging
console.log('flowChart filter ver. 0.7');
	log('definitions:');
	log('command: '+fcPrefix + fcCommand);
	log('optionBrackets: '+fcOptionBrackets+': '+fcOptionBrackets.charAt(0)+'...'+fcOptionBrackets.charAt(1));
	log('contentBrackets: '+fcContentBrackets+': '+fcContentBrackets.charAt(0)+'...'+fcContentBrackets.charAt(1));
	log('syntax: ' + fcPrefix + fcCommand + fcOptionBrackets.charAt(0) + '<options>' + fcOptionBrackets.charAt(1) + fcContentBrackets.charAt(0) + '<content>' + fcContentBrackets.charAt(1));


// initialize: set up context, content and defualt options
function initFlowcharts(options){
	fcHtmlContext = document.getElementsByTagName('body')[0];
	fcHtmlContent = fcHtmlContext.innerHTML;
	fcOptions = {
		'background-color' : 'rgba(255,255,255,0.75)',
		'align'            : 'center',
		'vertical-align'   : 'middle',
		'display'          : 'block',
		'x'                : 50,
		'y'                : 0,
		'line-width'       : 2,
		'line-length'      : 50,
		'text-margin'      : 10,
		'font-size'        : 14,
		'font-family'      : 'monospace',
		'font-color'       : 'black',
		'line-color'       : 'black',
		'element-color'    : 'black',
		'fill'             : 'none',
		'yes-text'         : 'yes',
		'no-text'          : 'no',
		'arrow-end'        : 'block-midium-long',
		'scale'            : 1,
		'flowstate'        : {
			'mark'  : {
				'fill'          : 'rgba(255,0,0,0.05)',
				'element-color' : 'red',
				'line-width'    : 4,
				'font-color'    : 'red',
				'font-weight'   : 'bold'
			},
			'blend' : {
				'element-color' : 'rgba(0,0,0,0.25)',
				'font-color'    : 'rgba(0,0,0,0.25)'
			},
			'none'  : {
				'fill'          : 'none',
				'element-color' : 'none'
			}
		}
	};
	// passing options
	for(var i in options){
		log("rewrite property '" + i + "': '" + fcOptions[i] + "' to '" + options[i] + "'");
		fcOptions[i] = parseInt(options[i]) || options[i];
	}
}


// parse options
function parseOptions(options){
	var newOptions = Object.create(fcOptions);
	if(options != null){
		var optionsArray = options.split(",");
		for(var i in optionsArray){
			if(optionsArray[i] == 'inline'){
				newOptions['display'] = 'inline-block';
				newOptions['align'] = 'none';
			}
			optionsArray[i] = optionsArray[i].replace("=", ":");
			//log("options: " + optionsArray[i]);
			if(optionsArray[i].match(/.:.*/)){
				var newOption = optionsArray[i].split(":");
				newOptions[newOption[0].trim()] = parseInt(newOption[1]) || newOption[1].trim();
			}
		}
	}
	return newOptions;
}


// find all flowchart commands
function findFlowcharts(){
	// find all \flowChart[...]{...} text
	// 2-d element is options string, 3-d --- content
	var reFind = new RegExp("\\" + fcPrefix + fcCommand + "(\\" + fcOptionBrackets.charAt(0) + "([^\\" + fcOptionBrackets.charAt(0) + "\\" + fcOptionBrackets.charAt(1) + "]*)\\" + fcOptionBrackets.charAt(1) + ")?" + fcContentBrackets.charAt(0) + "([^" + fcContentBrackets + "]*)" + fcContentBrackets.charAt(1) + "", "gm");
		log(reFind);
	// fill the array
	var i=0;
	while((fcContent[i]=reFind.exec(fcHtmlContent)) != null){
		fcContent[i][iContent] = parseContent(fcContent[i][iRawContent]);
		fcContent[i][iOptions] = parseOptions(fcContent[i][iRawOptions]);
		fcContent[i][iFlowchart] = fcCommand + i;
			log(i);
			log(fcContent[i][iCommand]);
			log(fcContent[i][iRawOptions]);
			log(fcContent[i][iRawContent]);
			log(fcContent[i][iOptions]);
			log(fcContent[i][iContent]);
		i++;
	}
}


// change all html symbols to proper
function parseContent(content){
	content = content.replace(/&gt;/g, '>');
	content = content.replace(/&lt;/g, '<');
	content = content.replace(/&nbsp;/g, ' ');
	content = content.replace(/&mdash;/g, '—');
	content = content.replace(/&ndash;/g, '–');
	content = content.replace(/<\/?p[^<>]*>/g, '');
	content = content.replace(/<p>/g, '');
	content = content.replace(/<br[^<>]*\/?>/g, '\n\n');
	content = content.replace(/^\s+|\s+$/gm, '\n');
	content = content.replace(/\n\n/gm, '\n');
	content = content.trim();
	return content;
}


// insert single div or span container instead of command
function insertContainer(content, container){
	fcHtmlContent = fcHtmlContent.replace(content, container);
	fcHtmlContext.innerHTML = fcHtmlContent;
}


// insert div or span containers instead of commad for all
// 	fcContent must be filled
function insertContainers(){
	var i = 0;
	while(fcContent[i] != null){
		insertContainer(fcContent[i][iCommand], "<div style='background-color:" + fcContent[i][iOptions]['background-color'] + "; display:" + fcContent[i][iOptions]['display'] + "; vertical-align:" + fcContent[i][iOptions]['vertical-align'] + "' align='" + fcContent[i][iOptions]['align'] + "' id='" + fcContent[i][iFlowchart] + "'></div>");
			log("drawing: " + fcContent[i][iFlowchart]);
		i++;
	}
}


// draw all charts
// 	all containers must be in a place
function drawFlowcharts(){
	var i = 0;
	while(fcContent[i] != null){
		var diagram = flowchart.parse(fcContent[i][iContent]);
		diagram.drawSVG(fcContent[i][iFlowchart], fcContent[i][iOptions]);
		i++;
	}
}


// do all the job
function filterFlowcharts(){
	if(fcHtmlContext == null){initFlowcharts()};
	findFlowcharts();
	insertContainers();
	drawFlowcharts();
}


// just debugging
function log(content){
	if(DEBUG){
		console.log(content);
	};
}
